---
layout: post
title:  "First post"
date:   2017-03-15 18:10:00 +0100
categories: updates
---
This is the first post for my website. Currently haven't got much here, and
will probable change the layout and design of this website. Stay tuned for more
updates!

{% highlight ruby %}
def print_hi(name)
  puts "This supports writing in #{name}"
end
print_hi('Ruby!')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}
